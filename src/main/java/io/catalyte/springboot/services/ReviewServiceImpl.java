package io.catalyte.springboot.services;

import io.catalyte.springboot.customExceptions.ResourceNotFound;
import io.catalyte.springboot.entities.Review;
import io.catalyte.springboot.entities.Vehicle;
import io.catalyte.springboot.repositories.ReviewRepository;
import io.catalyte.springboot.repositories.VehicleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ReviewServiceImpl implements ReviewService{


    private final ReviewRepository reviewRepository;
    private final VehicleRepository vehicleRepository;
    @Autowired
    public ReviewServiceImpl(ReviewRepository reviewRepository, VehicleRepository vehicleRepository) {
        this.reviewRepository = reviewRepository;
        this.vehicleRepository = vehicleRepository;
    }

    private Optional<Review> reviewOptional;

    private Review review;

    public List<Vehicle> vehicleList;
    private List<Review> reviewList = new ArrayList<>();

    @Override
    public List<Review> GetReviews() {
        try {
            reviewList = reviewRepository.findAll();
            return reviewList;
        } catch (DataAccessException dae) {
            throw new ResourceNotFound("Can't find list");
        }
    }

    @Override
    public Optional<Review> GetReviewById(Long id) {
        try {
            reviewOptional = reviewRepository.findById(id);
            return reviewOptional;
        } catch (DataAccessException dae) {
            throw new ResourceNotFound("Can't find list");
        }
    }


    @Override
    public void AddReview(Review review) {
        try {
            reviewRepository.save(review);

        } catch (DataAccessException dae) {
            throw new ResourceNotFound("Can't find list");
        }
    }

    @Override
    public void UpdateReview(Review review) {
        try {
            reviewRepository.save(review);

        } catch (DataAccessException dae) {
            throw new ResourceNotFound("Can't find list");
        }
    }

    @Override
    public void DeleteReview(Long id) {
        boolean exists = reviewRepository.existsById(id);
        if (!exists) {
            throw new ResourceNotFound("Can't find list");
        }
        reviewRepository.deleteById(id);
    }

    @Override
    public void DeleteAllReviewsByUsername(String username) {
        reviewRepository.deleteAllByUsername(username);
    }

    @Override
    public Long CountAllReviewsByMakeAndModel(String make, String model) {
        Long reviewCount;
        reviewCount = reviewRepository.countByVehicle_MakeAndVehicle_Model(make, model);
        return reviewCount;
    }

    @Override
    public int GetCountByRating (int rating) {
        try {
            int reviewCount = reviewRepository.countByRating(rating);
            return reviewCount;
        } catch (DataAccessException dae) {
            throw new ResourceNotFound("Can't find list");
        }
    }

    @Override
    public List<Review> GetReviewsByUsername (String username) {
        try {
            List<Review> reviewList = reviewRepository.findAllByUsername(username);
            return reviewList;
        } catch (DataAccessException dae) {
            throw new ResourceNotFound("Can't find list");
        }
    }
        @Override
    public List<Review> GetAllReviewsByVehicleMakeAndModel(String make, String model) {
        Long vehicleId;
        try {
            reviewList = reviewRepository.findByVehicle_MakeAndVehicle_Model(make, model);
            return reviewList;
        } catch (DataAccessException dae) {
            throw new ResourceNotFound("Can't find list");
        }
    }
}
