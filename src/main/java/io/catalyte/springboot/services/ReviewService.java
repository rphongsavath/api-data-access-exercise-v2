package io.catalyte.springboot.services;

import io.catalyte.springboot.entities.Review;
import java.util.List;
import java.util.Optional;

public interface ReviewService {

    List<Review> GetReviews();

    Optional<Review> GetReviewById(Long id);

    void AddReview(Review review);

    void UpdateReview(Review review);

    void DeleteReview(Long id);

    void DeleteAllReviewsByUsername(String username);

    Long CountAllReviewsByMakeAndModel(String make, String model);

    List<Review> GetAllReviewsByVehicleMakeAndModel(String make, String model);

    List<Review> GetReviewsByUsername (String username);

    int GetCountByRating (int rating);

}
