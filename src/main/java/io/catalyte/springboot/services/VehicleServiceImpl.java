package io.catalyte.springboot.services;

import io.catalyte.springboot.customExceptions.ResourceNotFound;
import io.catalyte.springboot.entities.Vehicle;
import io.catalyte.springboot.repositories.VehicleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class VehicleServiceImpl implements VehicleService{

    private final VehicleRepository vehicleRepository;
    @Autowired
    public VehicleServiceImpl(VehicleRepository vehicleRepository) {
        this.vehicleRepository = vehicleRepository;
    }
    List<Vehicle> vehicleList = new ArrayList<>();

    Optional<Vehicle> vehicleOptional;

    private Vehicle vehicle;

    @Override
    public List<Vehicle> GetVehicles() {

        try {
            vehicleList = vehicleRepository.findAll();
            return vehicleList;
        } catch (DataAccessException dae) {
            throw new ResourceNotFound("Can't find list");
        }
    }

    @Override
    public void AddVehicle(Vehicle vehicle) {
        try {
            vehicleRepository.save(vehicle);

        } catch (DataAccessException dae) {
            throw new ResourceNotFound("Can't find list");
        }
    }

    @Override
    public void UpdateVehicle(Vehicle vehicle) {
        try {
            vehicleRepository.save(vehicle);
        } catch (DataAccessException dae) {
            throw new ResourceNotFound("Can't find list");
        }
    }

    @Override
    public Optional<Vehicle> GetVehicleById(Long id) {
        try {
            vehicleOptional = vehicleRepository.findById(id);
            return vehicleOptional;
        } catch (DataAccessException dae) {
            throw new ResourceNotFound("Can't find list");
        }
    }

    @Override
    public List<Vehicle> FindAllByMakeAndModel(String make, String model) {
        try {
            vehicleList = vehicleRepository.findAllByMakeAndModel(make, model);
            return vehicleList;
        } catch (DataAccessException dae) {
            throw new ResourceNotFound("Can't find list");
        }
    }

    @Override
    public void DeleteVehicleById(Long id) {
        vehicleRepository.deleteById(id);
    }

    @Override
    public List<Vehicle> FindByTypeOrMakeOrModelOrYear(String type, String make, String model, Integer year) {
        vehicleList = vehicleRepository.findByTypeOrMakeOrModelOrYear(type, make, model, year);
        return vehicleList;
    }

    @Override
    public List<Vehicle>getVehiclesByMake(String make){
        try{
            vehicleList = vehicleRepository.findAllByMake(make);
            return vehicleList;
        }catch (DataAccessException dae) {
            throw new ResourceNotFound("Can't find list");
        }
    }

    @Override
    public List<Vehicle>getVehiclesByType(String type){
        try{
            vehicleList = vehicleRepository.findAllByType(type);
            return vehicleList;
        }catch (DataAccessException dae) {
            throw new ResourceNotFound("Can't find list");
        }
    }

    @Override
    public List<Vehicle>getVehiclesByModel(String model){
        try{
            vehicleList = vehicleRepository.findAllByModel(model);
            return vehicleList;
        }catch (DataAccessException dae){
            throw new ResourceNotFound("Can't find list");
        }
    }

    @Override
    public List<Vehicle>getVehiclesByYear(Integer year){
        try{
            vehicleList = vehicleRepository.findAllByYear(year);
            return vehicleList;
        }catch (DataAccessException dae){
            throw new ResourceNotFound("Can't find list");
        }
    }
    @Override
    public int getCountOfModel(String model){
        try{
            int vehicleCount = vehicleRepository.countByModel(model);
            return vehicleCount;
        }catch (DataAccessException dae){
            throw new ResourceNotFound("Can't find list");
        }
    }
}
