package io.catalyte.springboot.data;

import io.catalyte.springboot.entities.Review;
import io.catalyte.springboot.entities.Vehicle;
import io.catalyte.springboot.repositories.ReviewRepository;
import io.catalyte.springboot.repositories.VehicleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

@Component
public class DataLoader implements CommandLineRunner {

    @Autowired
    private ReviewRepository reviewRepository;
    @Autowired
    private VehicleRepository vehicleRepository;

    private Review review1;
    private Review review2;
    private Review review3;
    private Review review4;
    private Review review5;
    private Review review6;
    private Review review7;
    private Review review8;
    private Review review9;
    private Review review10;
    private Review review11;
    private Review review12;
    private Review review13;
    private Review review14;
    private Review review15;
    private Review review16;


    private Vehicle vehicle1;
    private Vehicle vehicle2;
    private Vehicle vehicle3;
    private Vehicle vehicle4;
    private Vehicle vehicle5;
    private Vehicle vehicle6;
    private Vehicle vehicle7;
    private Vehicle vehicle8;


    @Override
    public void run(String... args) throws Exception {
        loadVehicles();
        loadReviews();
    }

    private void loadVehicles() {
        vehicle1 = vehicleRepository.save(new Vehicle( "car", "Toyota", "Camry", 2017));
        vehicle3 = vehicleRepository.save(new Vehicle( "car", "Toyota", "Camry", 2023));
        vehicle2 = vehicleRepository.save(new Vehicle( "truck", "Ford", "F-150", 2022));
        vehicle4 = vehicleRepository.save(new Vehicle( "car", "Lexus", "EX460", 2015));
        vehicle5 = vehicleRepository.save(new Vehicle( "car", "Honda", "Accord", 2012));
        vehicle6 = vehicleRepository.save(new Vehicle( "truck", "Ford", "F-150", 2026));
        vehicle7 = vehicleRepository.save(new Vehicle( "car", "BMW", "7-series", 2016));
        vehicle8 = vehicleRepository.save(new Vehicle( "car", "BMW", "7-series", 2016));

    }

    private void loadReviews() {
        review1 = reviewRepository.save(new Review("Favorite Car",
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna ", 8, "1-1-2022", "carBuff", vehicle1));

        review2 = reviewRepository.save(new Review("Nice Truck",
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna ", 10, "5-17-2021", "truckGuy", vehicle2));

        review3 = reviewRepository.save(new Review("Hate this Car",
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna ", 4, "12-11-2022", "carHater", vehicle3));

        review4 = reviewRepository.save(new Review("Ok Truck",
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna ", 7, "3-21-2021", "BigBoy", vehicle2));

        review5 = reviewRepository.save(new Review("Favorite Car",
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna ", 8, "1-1-2022", "carBuff", vehicle4));

        review6 = reviewRepository.save(new Review("Nice Truck",
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna ", 10, "5-17-2021", "truckGuy", vehicle6));

        review7 = reviewRepository.save(new Review("Hate this Car",
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna ", 4, "12-11-2022", "carHater", vehicle3));

        review8 = reviewRepository.save(new Review("Ok Truck",
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna ", 7, "3-21-2021", "BigBoy", vehicle7));

        review9 = reviewRepository.save(new Review("Favorite Car",
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna ", 8, "1-1-2022", "carBuff", vehicle1));

        review10 = reviewRepository.save(new Review("Nice Truck",
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna ", 10, "5-17-2021", "truckGuy", vehicle2));

        review11 = reviewRepository.save(new Review("Hate this Car",
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna ", 4, "12-11-2022", "carHater", vehicle3));

        review12 = reviewRepository.save(new Review("Ok Truck",
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna ", 7, "3-21-2021", "BigBoy", vehicle2));

        review13 = reviewRepository.save(new Review("Favorite Car",
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna ", 8, "1-1-2022", "carBuff", vehicle5));

        review14 = reviewRepository.save(new Review("Nice Truck",
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna ", 10, "5-17-2021", "truckGuy", vehicle6));

        review15 = reviewRepository.save(new Review("Hate this Car",
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna ", 4, "12-11-2022", "carHater", vehicle8));

        review16 = reviewRepository.save(new Review("Ok Truck",
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna ", 7, "3-21-2021", "BigBoy", vehicle2));
    }
}
