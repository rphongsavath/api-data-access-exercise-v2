package io.catalyte.springboot.controllers;

import io.catalyte.springboot.entities.Vehicle;
import io.catalyte.springboot.services.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/vehicles")
public class VehicleController {

    @Autowired
    private VehicleService vehicleService;
    public List<Vehicle> vehicleList = new ArrayList<>();
    public Optional<Vehicle> vehicleOptional;
    public int vehicleCount;


    @GetMapping
    public List<Vehicle> getAllVehicles() {
        vehicleList = vehicleService.GetVehicles();
        return vehicleList;
    }

    @GetMapping(path = "/{id}")
    public Optional<Vehicle> getVehicleById(@PathVariable("id") Long id) {
        vehicleOptional = vehicleService.GetVehicleById(id);
        return vehicleOptional;
    }

    @GetMapping(path = "/byMakeAndModel")
    public List<Vehicle> GetVehicleByMakeAndModel(@RequestParam String make, String model) {
        vehicleList = vehicleService.FindAllByMakeAndModel(make, model);
        return vehicleList;
    }
    @PutMapping(path = "/{id}")
    public void updateVehicle(@PathVariable("id") Long id, @RequestBody Vehicle vehicle) {
        vehicleService.UpdateVehicle(vehicle);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void addVehicle(@RequestBody Vehicle vehicle) {
        vehicleService.AddVehicle(vehicle);
    }

    @DeleteMapping("/delete")
    @ResponseStatus(HttpStatus.OK)
    public void deleteVehicleById(@RequestParam Long id) {
        vehicleService.DeleteVehicleById(id);
    }
    @GetMapping(path = "/find")
    public List<Vehicle> findByTypeOrMakeOrModelOrYear(@RequestParam(required = false) String type, String make, String model, Integer year) {
        vehicleList = vehicleService.FindByTypeOrMakeOrModelOrYear(type, make, model, year);
        return vehicleList;
    }

    @GetMapping(path = "/byType")
    public List<Vehicle> GetVehiclesByType(@RequestParam String type) {
        vehicleList = vehicleService.getVehiclesByType(type);
        return vehicleList;
    }
    @GetMapping(path = "/byMake")
    public List<Vehicle> GetVehiclesByMake(@RequestParam String make) {
        vehicleList = vehicleService.getVehiclesByMake(make);
        return  vehicleList;
    }

    @GetMapping(path="/byModel")
    public List<Vehicle> GetVehiclesByModel(@RequestParam String model){
        vehicleList = vehicleService.getVehiclesByModel(model);
        return  vehicleList;
    }
    @GetMapping(path = "/byYear")
    public List<Vehicle> GetVehiclesByYear(@RequestParam Integer year){
        vehicleList = vehicleService.getVehiclesByYear(year);
        return vehicleList;
    }

    @GetMapping(path = "/countModel")
    public int GetCountOfModel(@RequestParam String model){
        vehicleCount = vehicleService.getCountOfModel(model);
        return vehicleCount;
    }


}
