package io.catalyte.springboot.controllers;

import io.catalyte.springboot.entities.Review;
import io.catalyte.springboot.services.ReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/reviews")
public class ReviewController {

    @Autowired
    private ReviewService reviewService;

    List<Review> reviewList = new ArrayList<>();

    Optional<Review> review;

    int reviewCount;

    @GetMapping
    public List<Review> getAllReviews() {
         reviewList = reviewService.GetReviews();
        return reviewList;
    }
    @GetMapping(path = "/{id}")
    public Optional<Review> getReviewById(@PathVariable("id") Long id) {
        review = reviewService.GetReviewById(id);
        return review;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void addReview(@RequestBody Review review) {
        reviewService.AddReview(review);
    }

    @PutMapping(path = "/{id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void updateReview(@RequestBody Review review, @PathVariable("id") Long id) {
        reviewService.UpdateReview(review);
    }
    @GetMapping(path = "/count")
    public Long CountAllReviewsByMakeAndModel(@RequestParam String make, String model) {
        Long reviewCount;
        reviewCount = reviewService.CountAllReviewsByMakeAndModel(make, model);
        return reviewCount;
    }
    @DeleteMapping(path = "username/{username}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    @Transactional
    public void deleteAllReviewsByUsername(@PathVariable("username") String username) {
        reviewService.DeleteAllReviewsByUsername(username);
    }

    @GetMapping(path = "/allByMakeAndModel")
    @ResponseStatus(HttpStatus.OK)
    public List<Review> getAllReviewsByMakeAndModel(@RequestParam String make, String model) {
        reviewList = reviewService.GetAllReviewsByVehicleMakeAndModel(make, model);
        return reviewList;
    }
    @DeleteMapping(path = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteReview(@PathVariable("id") Long id) {
        reviewService.DeleteReview(id);
    }

    @GetMapping(path="/byUsername")
    public List<Review> getReviewByUsername(@RequestParam String username){
        reviewList = reviewService.GetReviewsByUsername(username);
        return reviewList;
    }

    @GetMapping(path = "countByVehicleRating")
    public int getCountByRating(@RequestParam int rating){
        reviewCount = reviewService.GetCountByRating(rating);
        return reviewCount;
    }


}
