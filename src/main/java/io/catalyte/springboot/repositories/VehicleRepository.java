package io.catalyte.springboot.repositories;

import io.catalyte.springboot.entities.Vehicle;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VehicleRepository extends JpaRepository<Vehicle, Long> {

    List<Vehicle> findAllByMakeAndModel(String make, String model);
    List<Vehicle> findByTypeOrMakeOrModelOrYear(String type, String make, String model, Integer year);
    List<Vehicle> findAllByType(String type);
    List<Vehicle> findAllByMake(String make);
    List<Vehicle> findAllByModel(String model);
    List<Vehicle> findAllByYear(int Year);

    int countByModel(String model);

}
