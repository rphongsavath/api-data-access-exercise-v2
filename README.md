# API Data Access Exercise v2

***


## Name
Module 4 - API Data Access Exercise v2

## Description
This api application allows full CRUD and custom request for a used car review websites. Users will be able to make changes and queries to an establishing database.


## Installation
For installation, please have the latest version of PostgresSQL installed and confirm the application.yml file in the resource folder is updated as followed:
```
spring:
  jpa:
    database: POSTGRESQL
    show-sql: true
    hibernate:
      ddl-auto: create-drop
  datasource:
    platform: postgres
    url: jdbc:postgresql://localhost:5432/postgres
    username: postgres
    password: postgres
    driverClassName: org.postgresql.Driver
```

Next, please open the ServerRunner class in Intellij or equivalent platform and run the file. SpringBoot should run successfully and the data will populate the listing of vehicle and reviews once the server starts. If there are any issues with the dependencies, please open the POM file and confirm the dependencies are updated.
Next, please import the postman collection via the [attached link provided](https://orange-trinity-452585.postman.co/workspace/New-Team-Workspace~d9260c87-64ec-4e31-b095-8f670d4fa55d/collection/18114134-83005c7b-7306-49cd-bb46-660a271760f8?action=share&creator=25329122) and run the requests. The request listings can be autorun or individually selected and ran.

## Postman Collection Link
https://orange-trinity-452585.postman.co/workspace/New-Team-Workspace~d9260c87-64ec-4e31-b095-8f670d4fa55d/collection/18114134-83005c7b-7306-49cd-bb46-660a271760f8?action=share&creator=18114134

## Support
Please like, subscribe, and comment below if you have any questions or would like to provide any feedback.


## Contributing
The project is open to contributions and you must be part of the Create Opportunity program.


## Authors and acknowledgment
Don Moore and Richy Phongsavath are authors of this project as part of the Crate Opportunity program at Central Piedmont Community College.

## License
This application is open sourced and stored in a public GitLab repository.

## Project status
The project is completed unless specifically stated by review. 